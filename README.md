# Kubestrap

Bootstrapping an empty kubernetes cluster for my typical project needs:
a web-based service with SSL termination and certificates from letsencrypt.

Run as:

    EMAIL=<your@email.here> ./configure-cluster

It will create:

 - An nginx ingress (namespace `ingress-nginx`)
 - A cert manager (namespace `cert-manager`) with lets-encrypt issuers `staging` and `prod`


## How to take advantage of these in your services

For every service that you deploy that needs an external facing URL, you will
need to declare an `Ingress` resource for your `Service`.
It will look similar to this (fill out the placeholders with your own values):


    ---
    apiVersion: networking.k8s.io/v1
    kind: Ingress
    metadata:
      name: __INGRESS_NAME__  # e.g. kemendius-ui
      annotations:
        cert-manager.io/cluster-issuer: letsencrypt
        nginx.ingress.kubernetes.io/rewrite-target: /
    spec:
      rules:
        - host: __DOMAIN_NAME__   # e.g. kemendius.cat
          http:
            paths:
              - path: /
                backend:
                  service:
                    name: __SERVICE_NAME__  # your k8s Service resource, e.g. kemendius
                    port: 
                      number: 80
      tls: # < placing a host in the TLS config will indicate a certificate should be created
        - hosts:
            - __YOUR_DOMAIN_NAME__    # e.g. kemendius.cat
          # cert-manager will store the created certificate in this secret:
          secretName: __A_NAME__  # e.g. kemendius-cert


## TODOs

This is very simplitic to fulfill the needs of a couple of similar projects.
These scripts and configs will eventually become usable helm charts.
