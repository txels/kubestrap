#!/bin/bash
# Requires helm and kubectl

here=$(dirname $0)

if [ "${EMAIL}" == "" ]
then
    echo "Please define environment variables: EMAIL"
    exit -1
fi

# --- Set up helm repositories
# helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo add jetstack https://charts.jetstack.io
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx

# --- Deploy helm charts from repos

# cert-manager integrates letsencrypt
# https://hub.helm.sh/charts/jetstack/cert-manager/v0.13.1
helm upgrade --install --atomic cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --version v1.8.0 \
  --set installCRDs=true

helm upgrade --install ingress-nginx ingress-nginx/ingress-nginx \
  --namespace ingress-nginx --create-namespace \
  --set controller.ingressClassResource.default=true

# Some of these rely on the helm chart having been deployed
# (e.g. cert manager CRD)
# --- Manually apply global k8s configs
cat ./k8s/*.yml | sed "s/__EMAIL__/${EMAIL}/g" | kubectl apply -f -

