default:
    just --list

helm-plugins:
    helm plugin install https://github.com/databus23/helm-diff

helm-ls:
    helm list --all-namespaces

helm-find repo:
    helm search repo {{repo}} --versions
